$('.menu-icon').on('click', function () {
    if( !$('.mobile-menu').hasClass('active') ) {
        $(this).addClass('active')
        $('.mobile-menu').addClass('active');
    } else {
        $(this).removeClass('active')
        $('.mobile-menu').removeClass('active');
    }
});

$('.l-header .state-reports, .nav-state-reports').hover(
    function() {
        $('.l-header .case-studies').addClass('active');
        $('.nav-state-reports').addClass('active');
    }, function() {
        $('.l-header .case-studies').removeClass('active');
        $('.nav-state-reports').removeClass('active');
    }
);

$('.l-header .state-reports').on('click', function(e){
    e.preventDefault();
})

$('.more-case-studies .owl-carousel').owlCarousel({
    loop: false,
	rewind: true,
	nav: true,
    navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
    responsive : {
        // breakpoint from 0 up
        0 : {
            items: 1,
            slideBy: 1
        },
        // breakpoint from 768 up
        768 : {
            items: 3,
            slideBy: 3
        }
    }
});




$(function () {
    function scrollToAnchor(aid) {
        var offset = 180;
        var aTag = $("a[name='" + aid + "']");
        $('html, body').stop().animate({
            'scrollTop': aTag.offset().top - offset
        }, 500, 'swing', function() {
            // window.location.hash = target;
        });
    }

    if (window.location.href.indexOf("#digitally-driven_states") > -1) {
        $('.digitally-driven_states').trigger("click");
        setTimeout(() => {
            $('.digitally-driven_states-button').trigger("click");
            scrollToAnchor('digitally-driven_states-panel');
        }, 100);
    }
    $("#form-newsletter").submit(function(event){
        event.preventDefault(); //prevent default action 
        var post_url = $(this).attr("action"); //get form action url
        var request_method = $(this).attr("method"); //get form GET/POST method
        var form_data = $(this).serialize(); //Encode form elements for submission
        
        $.ajax({
            type: request_method,
            url: post_url,
            data: form_data,
            success: function() {
                $('#form-newsletter').hide();
                $('#form-messages').show();
            }
          });
    });
});

var acc = document.getElementsByClassName("accordion");
for (var i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}

$('.copyright-year').html(new Date().getFullYear());

var bg = [
    'USE Goodr_6.png',
    'NAMEGLO-CCC-7.jpg - COVER PHOTO.jpg',
    'NAMEGLO-CCC-14-edited.jpg',
    'Pietro-1 (1).jpg',
    'Pietro-4-lighter.jpg',
    'NAMEGLO-CCC-3.jpg',
    'NAMEGLO-CCC-16.jpg',
    'NH-Schaefer-56.jpg',
    'NH-Schaefer-64.jpg',
    'Pietro-44.jpg',
    'WA-Thompson-2107.jpg',
    'WA-Thompson-2247.jpg',
    'WA-Thompson-2537.jpg',
    'WA-Thompson-2627.jpg',
    'WA-Thompson-3215.jpg',
    'Facemask.jpg',
    'WA_Hero.jpg',
    'P1111312.jpg',
    'Pietro-34.jpg',
    'Pietro-47.jpg',
	'Pietro-54.jpg',
	'GA_Hero.jpg',
	'FL-Wilder-0005.jpg',
	'CC_CareAcademy00029.jpg',
	'okabashi_hero.jpg',
];

for (let index = 0; index < bg.length; index++) {
    const elem = bg[index];
    $('#set-image-' + index).css({
        "background-image" : "url('../images/backgrounds/" + elem + "')"   
    });
};

$('.goToCase').on('click', function () {
    var name = $(this).attr("name");
    window.location = 'their-stories/' + name + '.html';
});

